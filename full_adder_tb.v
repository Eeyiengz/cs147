`include "prj_definition.v"
module FULL_ADDER_TB;

wire S,CO;
reg A,B, CI;

FULL_ADDER full_a(S,CO,A,B, CI);

initial
begin
#10 A = 1'b0; B = 1'b0; CI = 1'b0;
#10 A = 1'b1; B = 1'b0; CI = 1'b0;
#10 A = 1'b0; B = 1'b1; CI = 1'b0;
#10 A = 1'b1; B = 1'b1; CI = 1'b0;

#10 A = 1'b0; B = 1'b0; CI = 1'b1;
#10 A = 1'b1; B = 1'b0; CI = 1'b1;
#10 A = 1'b0; B = 1'b1; CI = 1'b1;
#10 A = 1'b1; B = 1'b1; CI = 1'b1;
#10;

end
endmodule

