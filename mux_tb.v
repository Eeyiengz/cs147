`include "prj_definition.v"
module MUX1_2x1_TB;
reg I0, I1, S;
wire Y;

MUX1_2x1 mux1(.Y(Y), .I0(I0), .I1(I1), .S(S));

initial
begin
#10 I0 = 1'b0; I1 = 1'b1; S = 0; #10 $write("\nS=0; %d or %d is %d", I0, I1, Y);
#10 I0 = 1'b0; I1 = 1'b1; S = 1; #10 $write("\nS=1; %d or %d is %d\n", I0, I1, Y);
end
endmodule

module MUX32_2x1_TB;
reg [31:0] I0, I1;
reg S;
wire [31:0] Y;

MUX32_2x1 mux32_2(.Y(Y), .I0(I0), .I1(I1), .S(S));

initial
begin
#10 I0 = 0; I1 = 1; S = 0; #10 $write("\nS=0; %d or %d is %d", I0, I1, Y);
#10 I0 = 0; I1 = 1; S = 1; #10 $write("\nS=1; %d or %d is %d\n", I0, I1, Y);
end
endmodule

module MUX32_4x1_TB;
reg [31:0] I0, I1, I2, I3;
reg [1:0] S;
wire [31:0] Y;

MUX32_4x1 mux32_4(.Y(Y), .I0(I0), .I1(I1), .I2(I2), .I3(I3), .S(S));

initial
begin
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; S = 0; #10 $write("\nS=%d; %d", S, Y);
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; S = 1; #10 $write("\nS=%d; %d", S, Y);
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; S = 2; #10 $write("\nS=%d; %d", S, Y);
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; S = 3; #10 $write("\nS=%d; %d", S, Y);
end
endmodule


module MUX32_8x1_TB;
reg [31:0] I0, I1, I2, I3, I4, I5, I6, I7;
reg [2:0] S;
wire [31:0] Y;

MUX32_8x1 mux32_8(Y, I0, I1, I2, I3, I4, I5, I6, I7, S);

initial
begin
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; I4 = 4; I5 = 5; I6 = 6; I7 = 7; S = 0; #10 $write("\nS=%d; %d", S, Y);
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; I4 = 4; I5 = 5; I6 = 6; I7 = 7; S = 1; #10 $write("\nS=%d; %d", S, Y);
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; I4 = 4; I5 = 5; I6 = 6; I7 = 7; S = 2; #10 $write("\nS=%d; %d", S, Y);
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; I4 = 4; I5 = 5; I6 = 6; I7 = 7; S = 3; #10 $write("\nS=%d; %d", S, Y);

#10 S = 4; #10 $write("\nS=%d; %d", S, Y);
#10 S = 5; #10 $write("\nS=%d; %d", S, Y);
#10 S = 6; #10 $write("\nS=%d; %d", S, Y);
#10 S = 7; #10 $write("\nS=%d; %d", S, Y);
$write("\n");

end
endmodule


module MUX32_16x1_TB;
reg [31:0] I0, I1, I2, I3, I4, I5, I6, I7,
                     I8, I9, I10, I11, I12, I13, I14, I15;
reg [3:0] S;
wire [31:0] Y;

MUX32_16x1 mux32_16(Y, I0, I1, I2, I3, I4, I5, I6, I7,
                     I8, I9, I10, I11, I12, I13, I14, I15, S);

initial
begin
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; I4 = 4; I5 = 5; I6 = 6; I7 = 7; I8 = 8; I9 = 9; I10 = 10; I11 = 11; I12 = 12; I13 = 13; I14 = 14; I15 = 15;

#10 S = 0; #10 $write("\nS=%d; %d", S, Y);
#10 S = 1; #10 $write("\nS=%d; %d", S, Y);
#10 S = 2; #10 $write("\nS=%d; %d", S, Y);
#10 S = 3; #10 $write("\nS=%d; %d", S, Y);
#10 S = 4; #10 $write("\nS=%d; %d", S, Y);
#10 S = 5; #10 $write("\nS=%d; %d", S, Y);
#10 S = 6; #10 $write("\nS=%d; %d", S, Y);
#10 S = 7; #10 $write("\nS=%d; %d", S, Y);
#10 S = 8; #10 $write("\nS=%d; %d", S, Y);
#10 S = 9; #10 $write("\nS=%d; %d", S, Y);
#10 S = 10; #10 $write("\nS=%d; %d", S, Y);
#10 S = 11; #10 $write("\nS=%d; %d", S, Y);
#10 S = 12; #10 $write("\nS=%d; %d", S, Y);
#10 S = 13; #10 $write("\nS=%d; %d", S, Y);
#10 S = 14; #10 $write("\nS=%d; %d", S, Y);
#10 S = 15; #10 $write("\nS=%d; %d", S, Y);
$write("\n");
end
endmodule

module MUX32_32x1_TB;
reg [31:0] I0, I1, I2, I3, I4, I5, I6, I7,
                     I8, I9, I10, I11, I12, I13, I14, I15,
                     I16, I17, I18, I19, I20, I21, I22, I23,
                     I24, I25, I26, I27, I28, I29, I30, I31;
reg [4:0] S;
wire [31:0] Y;

MUX32_32x1 mux32_32(Y, I0, I1, I2, I3, I4, I5, I6, I7,
                     I8, I9, I10, I11, I12, I13, I14, I15, 
                     I16, I17, I18, I19, I20, I21, I22, I23,
                     I24, I25, I26, I27, I28, I29, I30, I31, S);

initial
begin
#10 I0 = 0; I1 = 1; I2 = 2; I3 = 3; I4 = 4; I5 = 5; I6 = 6; I7 = 7; I8 = 8; I9 = 9; I10 = 10; I11 = 11; I12 = 12; I13 = 13; I14 = 14; I15 = 15;
    I16 = 16; I17 = 17; I18 = 18; I19 = 19; I20 = 20; I21 = 21; I22 = 22; I23 = 23; I24 = 24; I25 = 25; I26 = 26; I27 = 27; I28 = 28; I29 = 29; I30 = 30; I31 = 31;

#10 S = 0; #10 $write("\nS=%d; %d", S, Y);
#10 S = 1; #10 $write("\nS=%d; %d", S, Y);
#10 S = 2; #10 $write("\nS=%d; %d", S, Y);
#10 S = 3; #10 $write("\nS=%d; %d", S, Y);
#10 S = 4; #10 $write("\nS=%d; %d", S, Y);
#10 S = 5; #10 $write("\nS=%d; %d", S, Y);
#10 S = 6; #10 $write("\nS=%d; %d", S, Y);
#10 S = 7; #10 $write("\nS=%d; %d", S, Y);
#10 S = 8; #10 $write("\nS=%d; %d", S, Y);
#10 S = 9; #10 $write("\nS=%d; %d", S, Y);
#10 S = 10; #10 $write("\nS=%d; %d", S, Y);
#10 S = 11; #10 $write("\nS=%d; %d", S, Y);
#10 S = 12; #10 $write("\nS=%d; %d", S, Y);
#10 S = 13; #10 $write("\nS=%d; %d", S, Y);
#10 S = 14; #10 $write("\nS=%d; %d", S, Y);
#10 S = 15; #10 $write("\nS=%d; %d", S, Y);
#10 S = 16; #10 $write("\nS=%d; %d", S, Y);
#10 S = 17; #10 $write("\nS=%d; %d", S, Y);
#10 S = 18; #10 $write("\nS=%d; %d", S, Y);
#10 S = 19; #10 $write("\nS=%d; %d", S, Y);
#10 S = 20; #10 $write("\nS=%d; %d", S, Y);
#10 S = 21; #10 $write("\nS=%d; %d", S, Y);
#10 S = 22; #10 $write("\nS=%d; %d", S, Y);
#10 S = 23; #10 $write("\nS=%d; %d", S, Y);
#10 S = 24; #10 $write("\nS=%d; %d", S, Y);
#10 S = 25; #10 $write("\nS=%d; %d", S, Y);
#10 S = 26; #10 $write("\nS=%d; %d", S, Y);
#10 S = 27; #10 $write("\nS=%d; %d", S, Y);
#10 S = 28; #10 $write("\nS=%d; %d", S, Y);
#10 S = 29; #10 $write("\nS=%d; %d", S, Y);
#10 S = 30; #10 $write("\nS=%d; %d", S, Y);
#10 S = 31; #10 $write("\nS=%d; %d", S, Y);

$write("\n");
end
endmodule
