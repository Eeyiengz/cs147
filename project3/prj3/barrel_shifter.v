// Name: barrel_shifter.v
// Module: SHIFT32_L , SHIFT32_R, SHIFT32
//
// Notes: 32-bit barrel shifter
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
`include "prj_definition.v"

// 32-bit shift amount shifter
module SHIFT32(Y,D,S, LnR);
// output list
output [31:0] Y;
// input list
input [31:0] D;
input [31:0] S;
input LnR;

wire [31:0] BS;
wire OR;
assign OR = |S[31:5];

BARREL_SHIFTER32 BS32(BS, D, S[4:0], LnR);

MUX32_2x1 mux(.Y(Y), .I0(BS), .I1(0), .S(OR));

endmodule

// Shift with control L or R shift
module BARREL_SHIFTER32(Y,D,S, LnR);
// output list
output [31:0] Y;
// input list
input [31:0] D;
input [4:0] S;
input LnR;

wire [31:0] R_S, L_S;

SHIFT32_R rs(R_S, D, S);
SHIFT32_L ls(L_S, D, S);

MUX32_2x1 mux_LR(.Y(Y), .I0(R_S), .I1(L_S), .S(LnR));

endmodule

// Right shifter
module SHIFT32_R(Y,D,S);
// output list
output [31:0] Y;
// input list
input [31:0] D;
input [4:0] S;

wire mux_S_B [3:0][31:0];

genvar i, j, k, p, q;

generate
	// col 0
	for (i = 31; i >= 0; i = i - 1) begin : LS_i_loop
		if (i == 31) begin MUX1_2x1 col00(.Y(mux_S_B[0][i]), .I0(D[i]), .I1(1'b0), .S(S[0])); end
		else begin MUX1_2x1 col01(.Y(mux_S_B[0][i]), .I0(D[i]), .I1(D[i+1]), .S(S[0])); end
	end
	
	//  col 1
	for (j = 31; j >= 0; j = j - 1) begin : LS_j_loop
		if (j >= 30) begin MUX1_2x1 col10(.Y(mux_S_B[1][j]), .I0(mux_S_B[0][j]), .I1(1'b0), .S(S[1])); end
		else begin MUX1_2x1 col11(.Y(mux_S_B[1][j]), .I0(mux_S_B[0][j]), .I1(mux_S_B[0][j+2]), .S(S[1])); end
	end

	// col 2
	for (k = 31; k >= 0; k = k - 1) begin : LS_k_loop
		if (k >= 28) begin MUX1_2x1 col20(.Y(mux_S_B[2][k]), .I0(mux_S_B[1][k]), .I1(1'b0), .S(S[2])); end
		else begin MUX1_2x1 col21(.Y(mux_S_B[2][k]), .I0(mux_S_B[1][k]), .I1(mux_S_B[1][k+4]), .S(S[2])); end
	end
	
	// col 3
	for (p = 31; p >= 0; p = p - 1) begin : LS_p_loop
		if (p >= 24) begin MUX1_2x1 col30(.Y(mux_S_B[3][p]), .I0(mux_S_B[2][p]), .I1(1'b0), .S(S[3])); end
		else begin MUX1_2x1 col31(.Y(mux_S_B[3][p]), .I0(mux_S_B[2][p]), .I1(mux_S_B[2][p+8]), .S(S[3])); end
	end

	// col 4
	for (q = 31; q >= 0; q = q - 1) begin : LS_q_loop
		if (q >= 16) begin MUX1_2x1 col40(.Y(Y[q]), .I0(mux_S_B[3][q]), .I1(1'b0), .S(S[4])); end
		else begin MUX1_2x1 col41(.Y(Y[q]), .I0(mux_S_B[3][q]), .I1(mux_S_B[3][q+16]), .S(S[4])); end
	end

endgenerate

endmodule

// Left shifter
module SHIFT32_L(Y,D,S);
// output list
output [31:0] Y;
// input list
input [31:0] D;
input [4:0] S;

wire mux_S_B [3:0][31:0];

genvar i, j, k, p, q;

generate
	// col 0
	for (i = 0; i < 32; i = i + 1) begin : LS_i_loop
		if (i == 0) begin MUX1_2x1 col00(.Y(mux_S_B[0][i]), .I0(D[i]), .I1(1'b0), .S(S[0])); end
		else begin MUX1_2x1 col01(.Y(mux_S_B[0][i]), .I0(D[i]), .I1(D[i-1]), .S(S[0])); end
	end
	
	//  col 1
	for (j = 0; j < 32; j = j + 1) begin : LS_j_loop
		if (j < 2) begin MUX1_2x1 col10(.Y(mux_S_B[1][j]), .I0(mux_S_B[0][j]), .I1(1'b0), .S(S[1])); end
		else begin MUX1_2x1 col11(.Y(mux_S_B[1][j]), .I0(mux_S_B[0][j]), .I1(mux_S_B[0][j-2]), .S(S[1])); end
	end

	// col 2
	for (k = 0; k < 32; k = k + 1) begin : LS_k_loop
		if (k < 4) begin MUX1_2x1 col20(.Y(mux_S_B[2][k]), .I0(mux_S_B[1][k]), .I1(1'b0), .S(S[2])); end
		else begin MUX1_2x1 col21(.Y(mux_S_B[2][k]), .I0(mux_S_B[1][k]), .I1(mux_S_B[1][k-4]), .S(S[2])); end
	end
	
	// col 3
	for (p = 0; p < 32; p = p + 1) begin : LS_p_loop
		if (p < 8) begin MUX1_2x1 col30(.Y(mux_S_B[3][p]), .I0(mux_S_B[2][p]), .I1(1'b0), .S(S[3])); end
		else begin MUX1_2x1 col31(.Y(mux_S_B[3][p]), .I0(mux_S_B[2][p]), .I1(mux_S_B[2][p-8]), .S(S[3])); end
	end

	// col 4
	for (q = 0; q < 32; q = q + 1) begin : LS_q_loop
		if (q < 16) begin MUX1_2x1 col40(.Y(Y[q]), .I0(mux_S_B[3][q]), .I1(1'b0), .S(S[4])); end
		else begin MUX1_2x1 col41(.Y(Y[q]), .I0(mux_S_B[3][q]), .I1(mux_S_B[3][q-16]), .S(S[4])); end
	end

endgenerate

endmodule

