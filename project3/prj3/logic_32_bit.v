// Name: logic_32_bit.v
// Module: 
// Input: 
// Output: 
//
// Notes: Common definitions
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 02, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
//
`include "prj_definition.v"
// 32-bit NOR
module NOR32_2x1(Y,A,B);
//output 
output [31:0] Y;
//input
input [31:0] A;
input [31:0] B;

genvar i;
generate
	for (i = 0; i < `DATA_WIDTH; i = i + 1) begin : nor32_2x1_loop
		nor nor32(Y[i], A[i], B[i]);
	end
endgenerate
endmodule

// 32-bit AND
module AND32_2x1(Y,A,B);
//output 
output [31:0] Y;
//input
input [31:0] A;
input [31:0] B;

genvar i;
generate
	for (i = 0; i < `DATA_WIDTH; i = i + 1) begin : and32_2x1_loop
		and and32(Y[i], A[i], B[i]);
	end
endgenerate
endmodule

// 32-bit inverter
module INV32_1x1(Y,A);
//output 
output [31:0] Y;
//input
input [31:0] A;

genvar i;
generate
	for (i = 0; i < `DATA_WIDTH; i = i + 1) begin : inv32_1x1_loop
		 not not32(Y[i], A[i]);
	end
endgenerate

endmodule

// 32-bit OR
module OR32_2x1(Y,A,B);
//output 
output [31:0] Y;
//input
input [31:0] A;
input [31:0] B;

wire [31:0] temp;

NOR32_2x1 nor32(.Y(temp), .A(A), .B(B));
INV32_1x1 inv32(.Y(Y), .A(temp));

endmodule
