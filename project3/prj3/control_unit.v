// Name: control_unit.v
// Module: CONTROL_UNIT
// Output: CTRL  : Control signal for data path
//         READ  : Memory read signal
//         WRITE : Memory Write signal
//
// Input:  ZERO : Zero status from ALU
//         CLK  : Clock signal
//         RST  : Reset Signal
//
// Notes: - Control unit synchronize operations of a processor
//          Assign each bit of control signal to control one part of data path
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
`include "prj_definition.v"
module CONTROL_UNIT(CTRL, READ, WRITE, ZERO, INSTRUCTION, CLK, RST); 
// Output signals
output [`CTRL_WIDTH_INDEX_LIMIT:0]  CTRL;
output READ, WRITE;

// input signals
input ZERO, CLK, RST;
input [`DATA_INDEX_LIMIT:0] INSTRUCTION;
 
wire [2:0] proc_state;
PROC_SM state_machine(.STATE(proc_state),.CLK(CLK),.RST(RST));

always @ (proc_state)
begin
   /* case (proc_state)
		`PROC_FETCH : ;
		`PROC_DECODE:;
		`PROC_EXE :;
		`PROC_MEM :;
		`PROC_WB :;
	*/
end
endmodule


//------------------------------------------------------------------------------------------
// Module: PROC_SM
// Output: STATE      : State of the processor
//         
// Input:  CLK        : Clock signal
//         RST        : Reset signal
//
// INOUT: MEM_DATA    : Data to be read in from or write to the memory
//
// Notes: - Processor continuously cycle witnin fetch, decode, execute, 
//          memory, write back state. State values are in the prj_definition.v
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
module PROC_SM(STATE,CLK,RST);
// list of inputs
input CLK, RST;
// list of outputs
output [2:0] STATE;

// TBD - take action on each +ve edge of clock

// current and next states
reg [2:0] STATE, NEXT_STATE;

// initiation of state 

initial
begin   
    STATE = 3'bxxx;   
    NEXT_STATE = `PROC_FETCH; 
end

// reset signal handling 
always @ (negedge RST) 
begin     
STATE = 3'bxxx;     
NEXT_STATE = `PROC_FETCH; 
end

// state switching 
always @(posedge CLK) 
begin
    STATE = NEXT_STATE;
    case (NEXT_STATE)
        `PROC_FETCH    : NEXT_STATE = `PROC_DECODE;
        `PROC_DECODE   : NEXT_STATE = `PROC_EXE;
        `PROC_EXE      : NEXT_STATE = `PROC_MEM;
        `PROC_MEM      : NEXT_STATE = `PROC_WB;
        `PROC_WB       : NEXT_STATE = `PROC_FETCH;
        default        : 
        begin
            STATE = 3'bxxx;
            NEXT_STATE = `PROC_FETCH;
        end
    endcase
end

endmodule
