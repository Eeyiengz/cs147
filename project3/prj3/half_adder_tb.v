
`include "prj_definition.v"

module HALF_ADDER_TB;

reg A, B;
wire Y, C;

HALF_ADDER h_adder(.Y(Y), .C(C), .A(A), .B(B));

initial
begin
#10 A = 1'b0; B = 1'b0;
#10 A = 1'b1; B = 1'b0;
#10 A = 1'b0; B = 1'b1;
#10 A = 1'b1; B = 1'b1;
#10;

end
endmodule
