`include "prj_definition.v"

module RC_ADD_SUB_32_TB;
reg [31:0] A, B;
reg SnA;
wire [31:0] Y;
wire CO;

RC_ADD_SUB_32 test32(.Y(Y), .CO(CO), .A(A), .B(B), .SnA(SnA));

initial
begin
#10 A = 10; B = 5; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 5; B = 15; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 6546551; B = 465465; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 0; B = 0; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 $write("\n");
#10 A = 10; B = 5; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 15; B = 15; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 6546551; B = 465465; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 0; B = 0; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 $write("\n");

end
endmodule


module RC_ADD_SUB_64_TB;
reg [63:0] A, B;
reg SnA;
wire [63:0] Y;
wire CO;

RC_ADD_SUB_64 test64(.Y(Y), .CO(CO), .A(A), .B(B), .SnA(SnA));

initial
begin
#10 A = 10; B = 5; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 5; B = 15; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 6546551; B = 465465; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 0; B = 0; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 $write("\n");
#10 A = 10; B = 5; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 15; B = 15; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 6546551; B = 465465; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 0; B = 0; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 $write("\n");

#10 A = 64'd9223372036854775807; B = 0; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 64'd9223372036854775807; B = 1; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 64'd9223372036854775807; B = -1; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 A = 0; B = 0; SnA = 0; #10 $write("\n%d + %d = %d, CO = %d", A, B, Y, CO);
#10 $write("\n");
#10 A = 64'd9223372036854775807; B = 5; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 64'd9223372036854775807; B = 0; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 64'd9223372036854775807; B = 64'd9000000000000000007; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 A = 0; B = 0; SnA = 1; #10 $write("\n%d - %d = %d, CO = %d", A, B, Y, CO);
#10 $write("\n");
end
endmodule