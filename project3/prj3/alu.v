// Name: alu.v
// Module: ALU
// Input: OP1[32] - operand 1
//        OP2[32] - operand 2
//        OPRN[6] - operation code
// Output: OUT[32] - output result for the operation
//
// Notes: 32 bit combinatorial ALU
// 
// Supports the following functions
//	- Integer add (0x1), sub(0x2), mul(0x3)
//	- Integer shift_rigth (0x4), shift_left (0x5)
//	- Bitwise and (0x6), or (0x7), nor (0x8)
//  - set less than (0x9)
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
//
`include "prj_definition.v"
module ALU(OUT, ZERO, OP1, OP2, OPRN);
// input list
input [`DATA_INDEX_LIMIT:0] OP1; // operand 1
input [`DATA_INDEX_LIMIT:0] OP2; // operand 2
input [`ALU_OPRN_INDEX_LIMIT:0] OPRN; // operation code

// output list
output [`DATA_INDEX_LIMIT:0] OUT; // result of the operation.
output ZERO;

wire [31:0] ALU_OUT;
wire [31:0] MUX_IN [15:0];
wire nSnA, aSnA, SnA;

assign ZERO = !(|OUT);

MULT32 mult32(.HI(), .LO(MUX_IN[3]), .A(OP1), .B(OP2));

SHIFT32 shift32(.Y(MUX_IN[4]), .D(OP1), .S(OP2), .LnR(OPRN[0]));

not not0(nSnA, OPRN[0]);
and and0(aSnA, OPRN[3], OPRN[0]);
or or0(SnA, nSnA, aSnA);
RC_ADD_SUB_32 add_sub32(.Y(MUX_IN[1]), .CO(), .A(OP1), .B(OP2), .SnA(SnA));

AND32_2x1 and32(.Y(MUX_IN[6]), .A(OP1), .B(OP2));

OR32_2x1 or32(.Y(MUX_IN[7]), .A(OP1), .B(OP2));

NOR32_2x1 nor32(.Y(MUX_IN[8]), .A(OP1), .B(OP2));


MUX32_16x1 op_out(.Y(OUT),
                     .I0(), .I1(MUX_IN[1]), .I2(MUX_IN[1]), .I3(MUX_IN[3]), .I4(MUX_IN[4]), .I5(MUX_IN[4]), .I6(MUX_IN[6]), .I7(MUX_IN[7]),
                     .I8(MUX_IN[8]), .I9({{31{1'b0}}, MUX_IN[1][31]}), .I10(), .I11(), .I12(), .I13(), .I14(), .I15(), .S(OPRN[3:0]));


endmodule
