// Name: register_file.v
// Module: REGISTER_FILE_32x32
// Input:  DATA_W : Data to be written at address ADDR_W
//         ADDR_W : Address of the memory location to be written
//         ADDR_R1 : Address of the memory location to be read for DATA_R1
//         ADDR_R2 : Address of the memory location to be read for DATA_R2
//         READ    : Read signal
//         WRITE   : Write signal
//         CLK     : Clock signal
//         RST     : Reset signal
// Output: DATA_R1 : Data at ADDR_R1 address
//         DATA_R2 : Data at ADDR_R1 address
//
// Notes: - 32 bit word accessible dual read register file having 32 regsisters.
//        - Reset is done at -ve edge of the RST signal
//        - Rest of the operation is done at the +ve edge of the CLK signal
//        - Read operation is done if READ=1 and WRITE=0
//        - Write operation is done if WRITE=1 and READ=0
//        - X is the value at DATA_R* if both READ and WRITE are 0 or 1
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
//
`include "prj_definition.v"

// This is going to be +ve edge clock triggered register file.
// Reset on RST=0
module REGISTER_FILE_32x32(DATA_R1, DATA_R2, ADDR_R1, ADDR_R2, 
                            DATA_W, ADDR_W, READ, WRITE, CLK, RST);

// input list
input READ, WRITE, CLK, RST;
input [`DATA_INDEX_LIMIT:0] DATA_W;
input [`REG_ADDR_INDEX_LIMIT:0] ADDR_R1, ADDR_R2, ADDR_W;

// output list
output [`DATA_INDEX_LIMIT:0] DATA_R1;
output [`DATA_INDEX_LIMIT:0] DATA_R2;

wire [31:0] D;
wire [31:0] LOAD;
wire [31:0] REG [31:0];
wire [31:0] R1_SEL, R2_SEL;
wire notR;
not not0(notR, RST);
DECODER_5x32 d(.D(D), .I(ADDR_W));

genvar i;

generate
	for (i = 0; i < 32; i = i + 1) begin : reg_file_loop
		and and0(LOAD[i], D[i], WRITE);
		REG32 reg_32(.Q(REG[i]), .D(DATA_W), .LOAD(LOAD[i]), .CLK(CLK), .RESET(RST));

	end
endgenerate
		MUX32_32x1 mux_R1(.Y(R1_SEL), .I0(REG[0]), .I1(REG[1]), .I2(REG[2]), .I3(REG[3]), .I4(REG[4]), .I5(REG[5]), .I6(REG[6]), .I7(REG[7]),
                    .I8(REG[8]), .I9(REG[9]), .I10(REG[10]), .I11(REG[11]), .I12(REG[12]), .I13(REG[13]), .I14(REG[14]), .I15(REG[15]),
                    .I16(REG[16]), .I17(REG[17]), .I18(REG[18]), .I19(REG[19]), .I20(REG[20]), .I21(REG[21]), .I22(REG[22]), .I23(REG[23]),
                    .I24(REG[24]), .I25(REG[25]), .I26(REG[26]), .I27(REG[27]), .I28(REG[28]), .I29(REG[29]), .I30(REG[30]), .I31(REG[31]), .S(ADDR_R1));

		MUX32_32x1 mux_R2(.Y(R2_SEL), .I0(REG[0]), .I1(REG[1]), .I2(REG[2]), .I3(REG[3]), .I4(REG[4]), .I5(REG[5]), .I6(REG[6]), .I7(REG[7]),
                    .I8(REG[8]), .I9(REG[9]), .I10(REG[10]), .I11(REG[11]), .I12(REG[12]), .I13(REG[13]), .I14(REG[14]), .I15(REG[15]),
                    .I16(REG[16]), .I17(REG[17]), .I18(REG[18]), .I19(REG[19]), .I20(REG[20]), .I21(REG[21]), .I22(REG[22]), .I23(REG[23]),
                    .I24(REG[24]), .I25(REG[25]), .I26(REG[26]), .I27(REG[27]), .I28(REG[28]), .I29(REG[29]), .I30(REG[30]), .I31(REG[31]), .S(ADDR_R2));
		
		MUX32_2x1 mux_R1_sel(.Y(DATA_R1), .I0(32'hzzzzzzzz), .I1(R1_SEL), .S(READ));
		MUX32_2x1 mux_R2_sel(.Y(DATA_R2), .I0(32'hzzzzzzzz), .I1(R2_SEL), .S(READ));
endmodule
