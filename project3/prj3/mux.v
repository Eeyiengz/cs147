// Name: mux.v
// Module: 
// Input: 
// Output: 
//
// Notes: Common definitions
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 02, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
//
// 32-bit mux
`include "prj_definition.v"
module MUX32_32x1(Y, I0, I1, I2, I3, I4, I5, I6, I7,
                     I8, I9, I10, I11, I12, I13, I14, I15,
                     I16, I17, I18, I19, I20, I21, I22, I23,
                     I24, I25, I26, I27, I28, I29, I30, I31, S);
// output list
output [31:0] Y;
//input list
input [31:0] I0, I1, I2, I3, I4, I5, I6, I7;
input [31:0] I8, I9, I10, I11, I12, I13, I14, I15;
input [31:0] I16, I17, I18, I19, I20, I21, I22, I23;
input [31:0] I24, I25, I26, I27, I28, I29, I30, I31;
input [4:0] S;


wire [31:0] W_015, W_1631;

MUX32_16x1 mux16_07_815(W_015, I0, I1, I2, I3, I4, I5, I6, I7, I8, I9, I10, I11, I12, I13, I14, I15, S[3:0]);
MUX32_16x1 mux16_1623_2431(W_1631, I16, I17, I18, I19, I20, I21, I22, I23, I24, I25, I26, I27, I28, I29, I30, I31, S[3:0]);
MUX32_2x1 mux2_015_1631(.Y(Y), .I0(W_015), .I1(W_1631), .S(S[4]));


endmodule

// 32-bit 16x1 mux
module MUX32_16x1(Y, I0, I1, I2, I3, I4, I5, I6, I7,
                     I8, I9, I10, I11, I12, I13, I14, I15, S);
// output list
output [31:0] Y;
//input list
input [31:0] I0;
input [31:0] I1;
input [31:0] I2;
input [31:0] I3;
input [31:0] I4;
input [31:0] I5;
input [31:0] I6;
input [31:0] I7;
input [31:0] I8;
input [31:0] I9;
input [31:0] I10;
input [31:0] I11;
input [31:0] I12;
input [31:0] I13;
input [31:0] I14;
input [31:0] I15;
input [3:0] S;

wire [31:0] W_07, W_815;

MUX32_8x1 mux8_01_23(.Y(W_07), .I0(I0), .I1(I1), .I2(I2), .I3(I3), .I4(I4), .I5(I5), .I6(I6), .I7(I7), .S(S[2:0]));
MUX32_8x1 mux8_45_67(.Y(W_815), .I0(I8), .I1(I9), .I2(I10), .I3(I11), .I4(I12), .I5(I13), .I6(I14), .I7(I15), .S(S[2:0]));
MUX32_2x1 mux2_03_47(.Y(Y), .I0(W_07), .I1(W_815), .S(S[3]));

endmodule

// 32-bit 8x1 mux
module MUX32_8x1(Y, I0, I1, I2, I3, I4, I5, I6, I7, S);
// output list
output [31:0] Y;
//input list
input [31:0] I0;
input [31:0] I1;
input [31:0] I2;
input [31:0] I3;
input [31:0] I4;
input [31:0] I5;
input [31:0] I6;
input [31:0] I7;
input [2:0] S;

wire [31:0] W_03, W_47;

MUX32_4x1 mux4_0_3(.Y(W_03), .I0(I0), .I1(I1), .I2(I2), .I3(I3), .S(S[1:0]));
MUX32_4x1 mux4_4_7(.Y(W_47), .I0(I4), .I1(I5), .I2(I6), .I3(I7), .S(S[1:0]));
MUX32_2x1 mux2_03_47(.Y(Y), .I0(W_03), .I1(W_47), .S(S[2]));

endmodule

// 32-bit 4x1 mux
module MUX32_4x1(Y, I0, I1, I2, I3, S);
// output list
output [31:0] Y;
//input list
input [31:0] I0;
input [31:0] I1;
input [31:0] I2;
input [31:0] I3;
input [1:0] S;

wire [31:0] W_01, W_23;

MUX32_2x1 mux2_0_1(.Y(W_01), .I0(I0), .I1(I1), .S(S[0]));
MUX32_2x1 mux2_2_3(.Y(W_23), .I0(I2), .I1(I3), .S(S[0]));
MUX32_2x1 mux2_01_23(.Y(Y), .I0(W_01), .I1(W_23), .S(S[1]));

endmodule

// 32-bit mux
module MUX32_2x1(Y, I0, I1, S);
// output list
output [31:0] Y;
//input list
input [31:0] I0;
input [31:0] I1;
input S;

genvar i;

generate
	for (i = 0; i < `DATA_WIDTH; i = i + 1) begin : mux32_2x1_loop
		MUX1_2x1 mux1(.Y(Y[i]), .I0(I0[i]), .I1(I1[i]), .S(S));
	end
endgenerate
endmodule

// 1-bit mux
module MUX1_2x1(Y, I0, I1, S);
//output list
output Y;
//input list
input I0, I1, S;

wire Y1, Y2, NS;

not not0(NS, S);
and and0(Y1, I1, S);
and and1(Y2, I0, NS);
or or0(Y, Y1, Y2);

endmodule
