// Name: logic.v
// Module: 
// Input: 
// Output: 
//
// Notes: Common definitions
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 02, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
//
// 64-bit two's complement
module TWOSCOMP64(Y,A);
//output list
output [63:0] Y;
//input list
input [63:0] A;
wire [63:0] temp;

genvar i;
generate
	for (i = 0; i < 64; i = i + 1) begin : inv64_1x1_loop
		 not not64(temp[i], A[i]);
	end
endgenerate

RC_ADD_SUB_64 add(.Y(Y), .CO(), .A(temp), .B(64'b1), .SnA(1'b0));
endmodule

// 32-bit two's complement
module TWOSCOMP32(Y,A);
//output list
output [31:0] Y;
//input list
input [31:0] A;

wire [31:0] temp;

INV32_1x1 inv32(.Y(temp), .A(A));
RC_ADD_SUB_32 add(.Y(Y), .CO(), .A(temp), .B(1), .SnA(1'b0));
endmodule

// 32-bit register +ve edge, Reset on RESET=0
module REG32(Q, D, LOAD, CLK, RESET);
output [31:0] Q;

input CLK, LOAD;
input [31:0] D;
input RESET;

genvar i;
generate
	for (i = 0; i < 32; i = i + 1) begin : reg_32_loop		
		REG1 reg0(.Q(Q[i]), .Qbar(), .D(D[i]), .L(LOAD), .C(CLK), .nP(1'b1), .nR(RESET));
	end
endgenerate
endmodule

// 1 bit register +ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module REG1(Q, Qbar, D, L, C, nP, nR);
input D, C, L;
input nP, nR;
output Q, Qbar;

wire mux_D;

MUX1_2x1 mux1(.Y(mux_D), .I0(Q), .I1(D), .S(L));
D_FF d_ff(.Q(Q), .Qbar(Qbar), .D(mux_D), .C(C), .nP(nP), .nR(nR));
endmodule

// 1 bit flipflop +ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module D_FF(Q, Qbar, D, C, nP, nR);
input D, C;
input nP, nR;
output Q,Qbar;

wire Y, Ybar, Cnot;

not not0(Cnot, C);

D_LATCH d(.Q(Y), .Qbar(Ybar), .D(D), .C(Cnot), .nP(nP), .nR(nR));
SR_LATCH sr(.Q(Q), .Qbar(Qbar), .S(Y), .R(Ybar), .C(C), .nP(nP), .nR(nR));
endmodule

// 1 bit D latch
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module D_LATCH(Q, Qbar, D, C, nP, nR);
input D, C;
input nP, nR;
output Q,Qbar;
wire not_D;

not not0(not_D, D);

SR_LATCH sr(Q, Qbar, D, not_D, C, nP, nR);

endmodule

// 1 bit SR latch
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module SR_LATCH(Q,Qbar, S, R, C, nP, nR);
input S, R, C;
input nP, nR;
output Q,Qbar;

wire sc, rc;

nand u(sc, S, C);
nand j(rc, R, C);
nand i(Q, sc, Qbar, nP);
nand k(Qbar, rc, Q, nR);

endmodule

// 5x32 Line decoder
module DECODER_5x32(D,I);
// output
output [31:0] D;
// input
input [4:0] I;

wire [15:0] Dx;
wire I4not;

DECODER_4x16 d(Dx, I[3:0]);

not not0(I4not, I[4]);

genvar i;
generate
	for (i = 0; i < 32; i = i + 1) begin : decoder_5x32_loop
		if (i < 16) begin and and0(D[i], Dx[i], I4not); end
		else begin and and0(D[i], Dx[i-16], I[4]); end
	end
endgenerate
endmodule

// 4x16 Line decoder
module DECODER_4x16(D,I);
// output
output [15:0] D;
// input
input [3:0] I;

wire [7:0] Dx;
wire I3not;

DECODER_3x8 d(Dx, I[2:0]);

not not0(I3not, I[3]);

genvar i;
generate
	for (i = 0; i < 16; i = i + 1) begin : decoder_4x16_loop
		if (i < 8) begin and and0(D[i], Dx[i], I3not); end
		else begin and and0(D[i], Dx[i-8], I[3]); end
	end
endgenerate
endmodule

// 3x8 Line decoder
module DECODER_3x8(D,I);
// output
output [7:0] D;
// input
input [2:0] I;

wire [3:0] Dx;
wire I2not;

DECODER_2x4 d(Dx, I[1:0]);

not not0(I2not, I[2]);

and and0(D[0], Dx[0], I2not);
and and1(D[1], Dx[1], I2not);
and and2(D[2], Dx[2], I2not);
and and3(D[3], Dx[3], I2not);

and and4(D[4], Dx[0], I[2]);
and and5(D[5], Dx[1], I[2]);
and and6(D[6], Dx[2], I[2]);
and and7(D[7], Dx[3], I[2]);

endmodule

// 2x4 Line decoder
module DECODER_2x4(D,I);
// output
output [3:0] D;
// input
input [1:0] I;

wire Inot [1:0];

not not0(Inot[0], I[0]);
not not1(Inot[1], I[1]);

and and0(D[0], Inot[1], Inot[0]);
and and1(D[1], Inot[1], I[0]);
and and2(D[2], I[1], Inot[0]);
and and3(D[3], I[1], I[0]);

endmodule
