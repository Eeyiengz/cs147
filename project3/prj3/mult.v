// Name: mult.v
// Module: MULT32 , MULT32_U
//
// Output: HI: 32 higher bits
//         LO: 32 lower bits
//         
//
// Input: A : 32-bit input
//        B : 32-bit input
//
// Notes: 32-bit multiplication
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
`include "prj_definition.v"

module MULT32(HI, LO, A, B);
// output list
output [31:0] HI;
output [31:0] LO;
// input list
input [31:0] A;
input [31:0] B;

wire [31:0] A_2C, B_2C, A_LINE, B_LINE, MULT_U_HI, MULT_U_LO, MULT_HI, MULT_LO;
wire SIGN;

TWOSCOMP32 A2C(.Y(A_2C), .A(A));
TWOSCOMP32 B2C(.Y(B_2C), .A(B));

MUX32_2x1 A_OPT(.Y(A_LINE), .I0(A), .I1(A_2C), .S(A[31]));
MUX32_2x1 B_OPT(.Y(B_LINE), .I0(B), .I1(B_2C), .S(B[31]));

xor xor0(SIGN, A[31], B[31]);
MULT32_U mult_u(.HI(MULT_U_HI), .LO(MULT_U_LO), .A(A_LINE), .B(B_LINE));

TWOSCOMP64 tc_64(.Y({MULT_HI, MULT_LO}), .A({MULT_U_HI, MULT_U_LO}));

MUX32_2x1 PROD_HI(.Y(HI), .I0(MULT_U_HI), .I1(MULT_HI), .S(SIGN));
MUX32_2x1 PROD_LO(.Y(LO), .I0(MULT_U_LO), .I1(MULT_LO), .S(SIGN));
endmodule



module MULT32_U(HI, LO, A, B);
// output list
output [31:0] HI;
output [31:0] LO;
// input list
input [31:0] A;
input [31:0] B;

wire CO_LIST [31:0];
wire [31:0] B_LIST [31:0];

AND32_2x1 and0(.Y(B_LIST[0]), .A(A), .B({32{B[0]}}));
buf b00(LO[0], B_LIST[0][0]);
buf b000(CO_LIST[0], 1'b0);

genvar i;
generate
	for (i = 1; i < 32; i = i + 1) begin : mult32_u_lo_loop
		wire [31:0] temp;
		AND32_2x1 ando(.Y(temp), .A(A), .B({32{B[i]}}));
		RC_ADD_SUB_32 adder(.Y(B_LIST[i]), .CO(CO_LIST[i]), .A(temp), .B({CO_LIST[i-1],{B_LIST[i-1][31:1]}}), .SnA(1'b0));
		buf bi(LO[i], B_LIST[i][0]);
	end
endgenerate

genvar k;
generate
	for (k = 0; k < 31; k = k + 1) begin : mult32_u_hi_loop
		buf bk(HI[k], B_LIST[31][k+1]);
	end
endgenerate
buf bk(HI[31], CO_LIST[31]);

endmodule
