`include "prj_definition.v"
module MULT32_U_TB;
reg [31:0] A, B;
wire [31:0] HI, LO;
wire signed [63:0] FULL;
assign FULL = {HI, LO};
MULT32_U mult_u(HI, LO, A, B);

initial
begin
#10 A = 10; B = 2; #10 $write("\n%d", FULL);
#10 A = 10000; B = 2000; #10 $write("\n%d", FULL);
#10 A = 1; B = 0; #10 $write("\n%d", FULL);
#10 A = -1; B = 1; #10 $write("\n%d", FULL);
#10 A = -1; B = -1; #10 $write("\n%d", FULL);
#10 A = 2147483645; B = 2147483645; #10 $write("\n%d", FULL);
#10 A = 2147483647; B = 2147483647; #10 $write("\n%d", FULL);
#10 A = 464; B = 234234; #10 $write("\nexpects: 108684576 %d\n", FULL);
#10 A = 4; B = 1000000000; #10 $write("\n%d\n", FULL);
end



endmodule

module MULT32_TB;
reg [31:0] A, B;
wire [31:0] HI, LO;
wire signed [63:0] FULL;
assign FULL = {HI, LO};
MULT32 mult(HI, LO, A, B);

initial
begin
#10 A = 10; B = 2; #10 $write("\n%d", FULL);
#10 A = 10000; B = 2000; #10 $write("\n%d", FULL);
#10 A = 1; B = 0; #10 $write("\n%d", FULL);
#10 A = -1; B = 1; #10 $write("\n%d", FULL);
#10 A = -1; B = -1; #10 $write("\n%d", FULL);
#10 A = 2147483645; B = 2147483645; #10 $write("\n%d", FULL);
#10 A = 2147483647; B = 2147483647; #10 $write("\n%d", FULL);
#10 A = 464; B = 234234; #10 $write("\n%d\n", FULL);

#10 A = 4; B = 1000000000; #10 $write("\n%d", FULL);
#10 A = -4; B = -1000000000; #10 $write("\n%d", FULL);
#10 A = -5; B = -1000000000; #10 $write("\n%d", FULL);
#10 A = -6; B = -1000000000; #10 $write("\n%d", FULL);
#10 A = -5; B = 100000; #10 $write("\n%d", FULL);
#10 A = 5; B = -100000; #10 $write("\n%d", FULL);
#10 A = 20000000; B = 2147483647; #10 $write("\n%d\n", FULL); 
#10 A = -1; B = 2147483647; #10 $write("\n%d\n\n", FULL);
end

endmodule