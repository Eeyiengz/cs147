// Name: control_unit.v
// Module: CONTROL_UNIT
// Output: RF_DATA_W  : Data to be written at register file address RF_ADDR_W
//         RF_ADDR_W  : Register file address of the memory location to be written
//         RF_ADDR_R1 : Register file address of the memory location to be read for RF_DATA_R1
//         RF_ADDR_R2 : Registere file address of the memory location to be read for RF_DATA_R2
//         RF_READ    : Register file Read signal
//         RF_WRITE   : Register file Write signal
//         ALU_OP1    : ALU operand 1
//         ALU_OP2    : ALU operand 2
//         ALU_OPRN   : ALU operation code
//         MEM_ADDR   : Memory address to be read in
//         MEM_READ   : Memory read signal
//         MEM_WRITE  : Memory write signal
//         
// Input:  RF_DATA_R1 : Data at ADDR_R1 address
//         RF_DATA_R2 : Data at ADDR_R1 address
//         ALU_RESULT    : ALU output data
//         CLK        : Clock signal
//         RST        : Reset signal
//
// INOUT: MEM_DATA    : Data to be read in from or write to the memory
//
// Notes: - Control unit synchronize operations of a processor
//
// Revision History:
//
// Version    Date        Who        email            note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014    Kaushik Patra    kpatra@sjsu.edu        Initial creation
//  1.1     Oct 19, 2014        Kaushik Patra   kpatra@sjsu.edu         Added ZERO status output
//------------------------------------------------------------------------------------------
`include "prj_definition.v"
module CONTROL_UNIT(MEM_DATA, RF_DATA_W, RF_ADDR_W, RF_ADDR_R1, RF_ADDR_R2, RF_READ, RF_WRITE,
                    ALU_OP1, ALU_OP2, ALU_OPRN, MEM_ADDR, MEM_READ, MEM_WRITE,
                    RF_DATA_R1, RF_DATA_R2, ALU_RESULT, ZERO, CLK, RST); 
// Output signals
// Outputs for register file 
output [`DATA_INDEX_LIMIT:0] RF_DATA_W;
output [`ADDRESS_INDEX_LIMIT:0] RF_ADDR_W, RF_ADDR_R1, RF_ADDR_R2;
output RF_READ, RF_WRITE;
// Outputs for ALU
output [`DATA_INDEX_LIMIT:0]  ALU_OP1, ALU_OP2;
output  [`ALU_OPRN_INDEX_LIMIT:0] ALU_OPRN;
// Outputs for memory
output [`ADDRESS_INDEX_LIMIT:0]  MEM_ADDR;
output MEM_READ, MEM_WRITE;

// Input signals
input [`DATA_INDEX_LIMIT:0] RF_DATA_R1, RF_DATA_R2, ALU_RESULT;
input ZERO, CLK, RST;

// Inout signal 
inout [`DATA_INDEX_LIMIT:0] MEM_DATA;
reg [`DATA_INDEX_LIMIT:0] DATA_RET;

assign MEM_DATA = ((MEM_READ === 1'b1) &&(MEM_WRITE === 1'b0)) ? {`DATA_WIDTH{1'bz} } : DATA_RET;

// Output registers 3.3.c
reg [`DATA_INDEX_LIMIT:0] RF_DATA_W;
reg [`ADDRESS_INDEX_LIMIT:0] RF_ADDR_W, RF_ADDR_R1, RF_ADDR_R2;
reg RF_READ, RF_WRITE;
// Outputs for ALU
reg [`DATA_INDEX_LIMIT:0]  ALU_OP1, ALU_OP2;
reg  [`ALU_OPRN_INDEX_LIMIT:0] ALU_OPRN;
// Outputs for memory
reg [`ADDRESS_INDEX_LIMIT:0]  MEM_ADDR;
reg MEM_READ, MEM_WRITE;

// internal registers
reg [`DATA_INDEX_LIMIT:0] PC_REG;
reg [`DATA_INDEX_LIMIT:0] INST_REG;
reg [`DATA_INDEX_LIMIT:0] SP_REF;

// ALU registers
reg [`ALU_OPRN_INDEX_LIMIT:0] OPCODE;
reg [`REG_ADDR_INDEX_LIMIT:0] RS;
reg [`REG_ADDR_INDEX_LIMIT:0] RT;
reg [`REG_ADDR_INDEX_LIMIT:0] RD;
reg [`REG_ADDR_INDEX_LIMIT:0] SHAMT;
reg [`ALU_OPRN_INDEX_LIMIT:0] FUNCT;
reg [15:0] IMM; // I type 16 bit immediate value
reg [`ADDRESS_INDEX_LIMIT:0] ADDR;
reg [`DATA_INDEX_LIMIT:0] SX_IMM, ZX_IMM, LUI_IMM;
reg [`DATA_INDEX_LIMIT:0] BR_ADDR;
reg [`DATA_INDEX_LIMIT:0] JMP_ADDR_32;

// State nets
wire [2:0] proc_state;

PROC_SM state_machine(.STATE(proc_state),.CLK(CLK),.RST(RST));

//------------------------------------------------------------------------------------------

// initialize PC and SP
initial
begin
    PC_REG = `INST_START_ADDR;
    SP_REF = `INIT_STACK_POINTER;
end

// changing of state, set signal values
always @ (proc_state)
begin
// TBD: Code for the control unit model 3.3.c
    
    case (proc_state)
        `PROC_FETCH : 
            begin
                MEM_ADDR = PC_REG;
                MEM_READ = 1'b1;
                MEM_WRITE = 1'b0;
                RF_READ = 1'b0;
                RF_WRITE = 1'b0;
            end
        `PROC_DECODE :
            begin
                INST_REG = MEM_DATA;
                print_instruction(INST_REG);
                
                // parse the instruction
                {OPCODE, RS, RT, RD, SHAMT, FUNCT} = INST_REG; // R-type
                {OPCODE, RS, RT, IMM} = INST_REG; // I-type
                {OPCODE, ADDR} = INST_REG;        // J-type
                
                // bit extension
                SX_IMM = {{16{IMM[15]}}, IMM};
                ZX_IMM = {{16{1'b0}}, IMM};
                LUI_IMM = {IMM, {16{1'b0}}};
                JMP_ADDR_32 = {{6{1'b0}}, ADDR};
                
                // RF operations
                RF_ADDR_R1 = RS;
                RF_ADDR_R2 = RT;
                RF_READ = 1'b1;    
                RF_WRITE = 1'b0;
            end
        `PROC_EXE : 
            begin
            case(OPCODE)
                // R-Type
                6'h00 : 
                    begin
                        ALU_OP1 = RF_DATA_R1;
                        ALU_OP2 = RF_DATA_R2;
                        case (FUNCT)
                            6'h20: begin ALU_OPRN = `ALU_OPRN_WIDTH'h01; end
                            6'h22: begin ALU_OPRN = `ALU_OPRN_WIDTH'h02; end
                            6'h2c: begin ALU_OPRN = `ALU_OPRN_WIDTH'h03; end
                            6'h24: begin ALU_OPRN = `ALU_OPRN_WIDTH'h06; end
                            6'h25: begin ALU_OPRN = `ALU_OPRN_WIDTH'h07; end
                            6'h27: begin ALU_OPRN = `ALU_OPRN_WIDTH'h08; end
                            6'h2a: begin ALU_OPRN = `ALU_OPRN_WIDTH'h09; end
                            6'h01: begin ALU_OPRN = `ALU_OPRN_WIDTH'h05; end
                            6'h02: begin ALU_OPRN = `ALU_OPRN_WIDTH'h04; end
                            6'h08: begin  end
                            default: $write("");
                        endcase
                    end
                // I-type
                6'h08 : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = SX_IMM; ALU_OPRN = `ALU_OPRN_WIDTH'h01; end 
                6'h1d : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = SX_IMM; ALU_OPRN = `ALU_OPRN_WIDTH'h03; end
                6'h0c : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = ZX_IMM; ALU_OPRN = `ALU_OPRN_WIDTH'h06; end 
                6'h0d : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = ZX_IMM; ALU_OPRN = `ALU_OPRN_WIDTH'h07; end
                6'h0f : begin  end 
                6'h0a : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = SX_IMM; ALU_OPRN = `ALU_OPRN_WIDTH'h09;  end
                6'h04 : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = RF_ADDR_R2; ALU_OPRN = `ALU_OPRN_WIDTH'h02; end
                6'h05 : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = RF_ADDR_R2; ALU_OPRN = `ALU_OPRN_WIDTH'h02; end
                6'h23 : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = SX_IMM; ALU_OPRN = `ALU_OPRN_WIDTH'h01; end
                6'h2b : begin ALU_OP1 = RF_DATA_R1; ALU_OP2 = SX_IMM; ALU_OPRN = `ALU_OPRN_WIDTH'h01; end
                // J-Type
                6'h02 : begin  end
                6'h03 : begin  end
                6'h1b : begin RF_ADDR_R1 = 'h0; ALU_OP1 = SP_REF; ALU_OP2 = 'h1; ALU_OPRN= 'h02; end
                6'h1c : begin RF_ADDR_R1 = 'h0; ALU_OP1 = SP_REF; ALU_OP2 = 'h1; ALU_OPRN= 'h01; end
                default: $write("");
            endcase
            end
        `PROC_MEM :
            begin
                // initial
                MEM_READ = 1'b0;
                MEM_WRITE = 1'b0;
                
                // 'lw', 'sw', 'push' and 'pop'
                case (OPCODE)
                    6'h23 : begin MEM_READ = 1'b1; MEM_WRITE = 1'b0; RF_ADDR_W = RT; MEM_ADDR = ALU_RESULT; RF_DATA_W = MEM_DATA; end
                    6'h2b : begin MEM_READ = 1'b0; MEM_WRITE = 1'b1; MEM_ADDR = ALU_RESULT; DATA_RET = RF_DATA_R2; end
                    6'h1b : begin MEM_READ = 1'b0; MEM_WRITE = 1'b1; MEM_ADDR = SP_REF; DATA_RET = RF_DATA_R1; SP_REF = ALU_RESULT; end
                    6'h1c : begin MEM_READ = 1'b1; MEM_WRITE = 1'b0; SP_REF = ALU_RESULT; MEM_ADDR = SP_REF; RF_DATA_W = MEM_DATA; end
                    default: $write("", OPCODE);
                endcase
            end
        `PROC_WB :
            begin
                // initial
                RF_READ = 1'b0;
                RF_WRITE = 1'b1;
                
                // Path selections
                case(OPCODE)
                    6'h00 : 
						begin 
							if (FUNCT != 6'h08) begin RF_ADDR_W = RD; RF_DATA_W = ALU_RESULT; end
                            else begin PC_REG = RF_DATA_R1; end
						end
                    6'h04 : begin if(ZERO === 1'b1 ALU_OP1 = PC_REG; ALU_OP2 = SX_IMM; ALU_OPRN = 'h01; PC_REG = ALU_RESULT; end
                    6'h05 : begin if(ZERO === 1'b0) ALU_OP1 = PC_REG; ALU_OP2 = SX_IMM; ALU_OPRN = 'h01; PC_REG = ALU_RESULT; end
                    6'h2b : begin  end
                    6'h02 : begin PC_REG = JMP_ADDR_32 - 1; end
                    6'h03 : begin RF_ADDR_W = 31; RF_DATA_W = PC_REG + 1; PC_REG = JMP_ADDR_32 - 1; end
                    6'h1b : begin SP_REF = ALU_RESULT; end
                    6'h1c : begin RF_ADDR_W = 0; RF_DATA_W = MEM_DATA; end
                    6'h0f : begin RF_ADDR_W = RT; RF_DATA_W = LUI_IMM; end
                    6'h23 : begin  end
                    default : begin RF_ADDR_W = RT; RF_DATA_W = ALU_RESULT; end
                endcase
                
                PC_REG = PC_REG + 1;
                
            end
    endcase
end
//------------------------------------------------------------------------------------------

task print_instruction;
input [`DATA_INDEX_LIMIT:0] INST;
begin
    $write("@ %6dns -> [0X%08h] ", $time, INST);

    case(OPCODE)
    // R-Type
        6'h00 : begin
                    case(FUNCT)
                        6'h20: $write("add  r%02d, r%02d, r%02d;", RD, RS, RT);
                        6'h22: $write("sub  r%02d, r%02d, r%02d;", RD, RS, RT);
                        6'h2c: $write("mul  r%02d, r%02d, r%02d;", RD, RS, RT);
                        6'h24: $write("and  r%02d, r%02d, r%02d;", RD, RS, RT);
                        6'h25: $write("or   r%02d, r%02d, r%02d;", RD, RS, RT);
                        6'h27: $write("nor  r%02d, r%02d, r%02d;", RD, RS, RT);
                        6'h2a: $write("slt  r%02d, r%02d, r%02d;", RD, RS, RT);
                        6'h01: $write("sll  r%02d, r%02d, 0X%02h;", RD, RS, SHAMT);
                        6'h02: $write("srl  r%02d, r%02d, 0X%02h;", RD, RS, SHAMT);
                        6'h08: $write("jr   r%02d;", RS);
                        default: $write("");
                    endcase
                end
        // I-type
        6'h08 : $write("addi  r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        6'h1d : $write("muli  r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        6'h0c : $write("andi  r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        6'h0d : $write("ori   r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        6'h0f : $write("lui   r%02d, 0X%04h;", RT, IMM);
        6'h0a : $write("slti  r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        6'h04 : $write("beq   r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        6'h05 : $write("bne   r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        6'h23 : $write("lw    r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        6'h2b : $write("sw    r%02d, r%02d, 0X%04h;", RT, RS, IMM);
        // J-Type
        6'h02 : $write("jmp   0X%07h;", JMP_ADDR_32);
        6'h03 : $write("jal   0X%07h;", JMP_ADDR_32);
        6'h1b : $write("push;");
        6'h1c : $write("pop;");
        default: $write("");
    endcase
    $write("\n");
end
endtask
endmodule

//------------------------------------------------------------------------------------------
// Module: CONTROL_UNIT
// Output: STATE      : State of the processor
//         
// Input:  CLK        : Clock signal
//         RST        : Reset signal
//
// INOUT: MEM_DATA    : Data to be read in from or write to the memory
//
// Notes: - Processor continuously cycle witnin fetch, decode, execute, 
//          memory, write back state. State values are in the prj_definition.v
//
// Revision History:
//
// Version    Date        Who        email            note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014    Kaushik Patra    kpatra@sjsu.edu        Initial creation
//------------------------------------------------------------------------------------------
module PROC_SM(STATE,CLK,RST);
// list of inputs
input CLK, RST;
// list of outputs
output [2:0] STATE;

// TBD - implement the state machine here 3.3.b

// current and next states
reg [2:0] STATE, NEXT_STATE;

// initiation of state 
initial 
begin   
    STATE = 3'bxxx;   
    NEXT_STATE = `PROC_FETCH; 
end

// reset signal handling 
always @ (negedge RST) 
begin     
STATE = 3'bxxx;     
NEXT_STATE = `PROC_FETCH; 
end

// state switching 
always @(posedge CLK) 
begin
    STATE = NEXT_STATE;
    case (NEXT_STATE)
        `PROC_FETCH    : NEXT_STATE = `PROC_DECODE;
        `PROC_DECODE: NEXT_STATE = `PROC_EXE;
        `PROC_EXE    : NEXT_STATE = `PROC_MEM;
        `PROC_MEM    : NEXT_STATE = `PROC_WB;
        `PROC_WB    : NEXT_STATE = `PROC_FETCH;
        default        : 
        begin
            STATE = 3'bxxx;
            NEXT_STATE = `PROC_FETCH;
        end
    endcase
end
// end of 3.3.b
endmodule
