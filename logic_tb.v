`include "prj_definition.v"

module TWOSCOMP32_TB;
reg [31:0] A;
wire [31:0] Y;

TWOSCOMP32 twosC(.Y(Y), .A(A));

initial
begin
#10 A = 1111; #10 $write("\n%d", Y);
#10 A = 0; #10 $write("\n%d", Y);
#10 A = -1111; #10 $write("\n%d\n", Y);
end
endmodule


module TWOSCOMP64_TB;
reg [63:0] A;
wire [63:0] Y;

TWOSCOMP64 twosC64(.Y(Y), .A(A));

initial
begin
#10 A = 64'd1111111111311111; #20 $write("\n%d", A);
#10 A = 0; #200 $write("\n%d", Y);
#10 A = -64'd1; #200 $write("\n%d", Y);
#10 A = 64'd1111; #2000 $write("\n%d\n", Y);
end
endmodule

module SR_LATCH_TB;
reg S, R, C, nP, nR;
wire Q, Qbar;

SR_LATCH sr(Q, Qbar, S, R, C, nP, nR);

initial
begin
#10 C = 0; 

end
endmodule

module D_FF_TB;
reg D, C, nP, nR;
wire Q, Qbar;

D_FF b(.Q(Q), .Qbar(Qbar), .D(D), .C(C), .nP(nP), .nR(nR));

initial 
begin

#10 $write("Q: ", Q); #10 nP = 0; nR = 0; #10 $display("; Q+1: ", Q);
#10 $write("Q: ", Q); #10 nP = 0; nR = 1; #10 $display("; Q+1: ", Q);
#10 $write("Q: ", Q); #10 nP = 1; nR = 0; #10 $display("; Q+1: ", Q);

#10 $write("Q: ", Q); #10 C = 0; nP = 1; nR = 1; #10 $display("; Q+1: ", Q);
#10 $write("Q: ", Q); #10 C = 0; nP = 1; nR = 1; #10 $display("; Q+1: ", Q);

#10 $write("Q: ", Q); #10 C = 1; D = 0; nP = 1; nR = 1; #10 $display("; Q+1: ", Q);
#10 $write("Q: ", Q); #10 C = 1; D = 1; nP = 1; nR = 1; #10 $display("; Q+1: ", Q);

end
endmodule 

module REG1_TB;
reg D, L, C, nP, nR;
wire Q, Qbar;

REG1 reg1(.Q(Q), .Qbar(Qbar), .D(D), .L(L), .C(C), .nP(nP), .nR(nR));
initial
begin
nR = 1'b0; nP = 1'b1;
#10 nR = 1'b1; nP = 1'b0; #10 $display("Q: ", Q, "; Qbar: ", Qbar);
#10 nR = 1'b1; nP = 1'b1; #10 $display("Q: ", Q, "; Qbar: ", Qbar);
#10 D = 1'b1; L = 1'b0; C = 1'b0; #10 $display("Q: ", Q, "; Qbar: ", Qbar);
#10 D = 1'b1; L = 1'b0; C = 1'b1; #10 $display("Q: ", Q, "; Qbar: ", Qbar);
#10 D = 1'b1; L = 1'b1; C = 1'b0; #10 $display("Q: ", Q, "; Qbar: ", Qbar);
#10 D = 1'b1; L = 1'b1; C = 1'b1; #10 $display("Q: ", Q, "; Qbar: ", Qbar);
#10 D = 1'b0; L = 1'b0; C = 1'b0; #10 $display("Q: ", Q, "; Qbar: ", Qbar);
#10 D = 1'b0; L = 1'b0; C = 1'b1; #10 $display("Q: ", Q, "; Qbar: ", Qbar); 
#10 D = 1'b0; L = 1'b0; C = 1'b1; #10 $display("Q: ", Q, "; Qbar: ", Qbar); 
#10 D = 1'b0; L = 1'b1; C = 1'b1; #10 $display("Q: ", Q, "; Qbar: ", Qbar); 
end
endmodule 
