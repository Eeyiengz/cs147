`include "prj_definition.v"

// NOR32 test
module NOR32_2x1_TB;
reg [31:0] A, B;
wire [31:0] Y;

NOR32_2x1 nor32(.Y(Y),.A(A),.B(B));

initial
begin
#10 A = 35154; B = 443531; #10 $write("\n%d", Y);
#10 A = -1; B = 0; #10 $write("\n%d", Y);
#10 A = -1; B = -1; #10 $write("\n%d", Y);
#10 A = 0; B = 0; #10 $write("\n%d", Y);
end
endmodule


// AND32 test
module AND32_2x1_TB;
reg [31:0] A, B;
wire [31:0] Y;

AND32_2x1 and32(.Y(Y),.A(A),.B(B));

initial
begin
#10 A = 35154; B = 443531; #10 $write("\n%d", Y);
#10 A = -1; B = 0; #10 $write("\n%d", Y);
#10 A = -1; B = -1; #10 $write("\n%d", Y);
#10 A = 0; B = 0; #10 $write("\n%d", Y);
end
endmodule


// INV32 test
module INV32_1x1_TB;
reg [31:0] A, B;
wire [31:0] Y;

INV32_1x1 inv32(.Y(Y),.A(A));

initial
begin
#10 A = 3515454548; #10 $write("\n%d", Y);
#10 A = -1; #10 $write("\n%d", Y);
#10 A = 4294967295; #10 $write("\n%d", Y);
#10 A = 0; #10 $write("\n%d", Y);
end
endmodule


// OR32 test
module OR32_2x1_TB;
reg [31:0] A, B;
wire [31:0] Y;

OR32_2x1 or32(.Y(Y),.A(A),.B(B));

initial
begin
#10 A = 35154; B = 443531; #10 $write("\n%d", Y);
#10 A = -1; B = 0; #10 $write("\n%d", Y);
#10 A = -1; B = -1; #10 $write("\n%d", Y);
#10 A = 0; B = 0; #10 $write("\n%d", Y);
end
endmodule